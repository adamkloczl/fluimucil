﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour {
    public static DeathMenu instance;

    public string BackToMenu;

    public string NewGame;

    public bool isPaused;

    public GameObject DeathMenuCanvas;

    void Start()
    {
        instance = this;
        DeathMenuCanvas.SetActive(false);
    }

    public void AppearOnDeath()
    {
        DeathMenuCanvas.SetActive(true);
    }

    public void DisappearOnRevive()
    {
        DeathMenuCanvas.SetActive(false);
    }

    /*public void Pause()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        DeathMenuCanvas.SetActive(true);
    }

    public void Resume()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        DeathMenuCanvas.SetActive(false);
    }*/

    public void StartNewGame()
    {
        SceneManager.LoadScene(NewGame);
        DeathMenuCanvas.SetActive(false);
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(BackToMenu);
    }

    public void quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
