﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public string NewGame;

    public string Tips;

    public string BackToMenu;

    //public string Support;


    public void StartNewGame()
    {
        SceneManager.LoadScene(NewGame);
    }

    public void tips()
    {
        SceneManager.LoadScene(Tips);
    }

    /*public void credits()
    {
        SceneManager.LoadScene(Support);
    }*/

    public void quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(BackToMenu);
    }

}
