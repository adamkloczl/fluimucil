﻿using UnityEngine;
using System.Collections;

public class FluiSpawner : MonoBehaviour {
    public static FluiSpawner instance;

    public GameObject FluiGO; //prefab
    public GameObject FluiKékGO;

    float maxSpawnRatePerSeconds = 4f;

    // Use this for initialization
    void Start()
    {
        instance = this;
        Invoke("SpawnFlui", maxSpawnRatePerSeconds);
    }

    //function to spawn enemy
    void SpawnFlui()
    {
            Spawn(FluiGO);
            //schedule when to spawn next enemy
            ScheduleNextFluiSpawn();
    }
    public void SpawnFluiKek()
    {
        Spawn(FluiKékGO);
    }

    void ScheduleNextFluiSpawn()
    {
        Invoke("SpawnFlui", maxSpawnRatePerSeconds);
        
    }

    void Spawn(GameObject gameObject)
    {
        //this is the bottom-left point of the screen
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, -1));

        //this is the topright point of the screen
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 2));

        //instantiate a flui
        GameObject newGameObject = (GameObject)Instantiate(gameObject);
        newGameObject.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
    }
}
