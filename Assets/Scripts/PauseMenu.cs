﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour {

    public static InGameMenu instance;
    
    public string BackToMenu;

    public string NewGame;

    public bool isPaused;

    public GameObject pauseMenuCanvas;

    void Start()
    {
        instance = this;
        pauseMenuCanvas.SetActive(false);
    }

    public void Pause()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        pauseMenuCanvas.SetActive(true);
    }

    public void Resume()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        pauseMenuCanvas.SetActive(false);
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene(NewGame);
        pauseMenuCanvas.SetActive(false);
        Time.timeScale = 1;
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(BackToMenu);
    }

    public void quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
