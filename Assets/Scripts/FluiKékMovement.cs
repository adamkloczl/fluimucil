﻿using UnityEngine;
using System.Collections;

public class FluiKékMovement : MonoBehaviour {

    float speed;

    // Use this for initialization
    void Start()
    {
        speed = 14f;
    }

    // Update is called once per frame
    void Update()
    {
        //Get the current enemy position
        Vector2 position = transform.position;

        //Compute the enemy new position
        position = new Vector2(position.x, position.y - speed * Time.deltaTime);

        //Update the enemy position
        transform.position = position;

        //this is the bottom-left point of the screen
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, -1));

        //if the enemy went outside the screen on the bottom, then destroy the enemy
        if (transform.position.y < min.y)
        {
            Destroy(gameObject);
        }

        //Game difficulty +
        if (ScoreHelper.instance.score >= 10)
        {
            speed = 16;
        }
    }
}
