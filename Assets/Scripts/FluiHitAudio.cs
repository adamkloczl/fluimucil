﻿using UnityEngine;
using System.Collections;

public class FluiHitAudio : MonoBehaviour
{
    private AudioManager audioManager;

    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("AudioManager not found");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //knife hit
        if (other.gameObject.tag == "Player")
        {
            if (ScoreHelper.instance.score % 2 == 0)
            {
                audioManager.PlaySound("why1");
            }
            else
            {
                audioManager.PlaySound("why2");
            }
        }

    }
}