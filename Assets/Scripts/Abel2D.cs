﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

namespace UnitySampleAssets._2D
{

    public class Abel2D : MonoBehaviour
    {
        public static Abel2D instance;

        public int currentPlayerHealth;
        public int maxPlayerHealth;
        public Image HealthBar;

        [SerializeField]
        private float maxSpeed; // The fastest the player can travel in the x axis.

        Rigidbody2D rb2D;

        bool currentPlatformAndroid = false;

        void Awake()
        {
            //Assigning variables
            currentPlayerHealth = 5;
            maxPlayerHealth = 5;
            instance = this;
            rb2D = GetComponent<Rigidbody2D>();

                    #if UNITY_ANDROID
                        currentPlatformAndroid = true;
                    #else
                        currentPlatformAndroid = false;
                    #endif
        }

        void Start()
        {
            Time.timeScale = 1;

            if(currentPlatformAndroid == true)
            {
                Debug.Log("Android");
            }
            else
            {
                Debug.Log("Windows");
            }
        }

        void Update()
        {
            //HP Check
            HealthBar.fillAmount = (float)currentPlayerHealth / maxPlayerHealth;
            if (currentPlayerHealth <= 0)
            {
                Time.timeScale = 0;
                DeathMenu.instance.AppearOnDeath();
            }
            //move Ábel
            if (currentPlatformAndroid == true)
            {
                //Move him with touch
                //TouchMove();
            }
            else
            {
                //Move him with keyboard
                Vector2 moveDir = new Vector2(Input.GetAxisRaw("Horizontal") * maxSpeed, rb2D.velocity.y);
                rb2D.velocity = moveDir;

                if (Input.GetAxisRaw("Horizontal") == 1)
                {
                    transform.localScale = new Vector3(7, 8, 1);
                }

                else if (Input.GetAxisRaw("Horizontal") == -1)
                {
                    transform.localScale = new Vector3(-7, 8, 1);
                }
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            //knife hit
            if (other.gameObject.tag == "Kés")
            {
                currentPlayerHealth -= 1;
                Destroy(other.gameObject);
            }
            //blue flui pickup
            else if (other.tag == "fluikék")
            {
                //ScoreHelper.instance.Addpoints();
                currentPlayerHealth += 1;
                if (currentPlayerHealth > maxPlayerHealth)
                {
                    currentPlayerHealth = maxPlayerHealth;
                }
                Destroy(other.gameObject);
            }
            // flui pickup
            else if (other.tag == "flui")
            {
                ScoreHelper.instance.Addpoints();
                Destroy(other.gameObject);
            }
        }

        public void revivePlayer()
        {
            DeathMenu.instance.DisappearOnRevive();
            Time.timeScale = 0;
            InGameMenu.instance.pauseMenuCanvas.SetActive(true);
            currentPlayerHealth = maxPlayerHealth;
            ReviveCounter.instance.ReviveCount += 1;
            DestroyGameObjectsWithTag("Kés");
            DestroyGameObjectsWithTag("flui");
            DestroyGameObjectsWithTag("fluikék");
        }

        public static void DestroyGameObjectsWithTag(string tag)
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);

            foreach (GameObject Kés in gameObjects)
            {
                GameObject.Destroy(Kés);
            }

            foreach (GameObject flui in gameObjects)
            {
                GameObject.Destroy(flui);
            }

            foreach (GameObject fluikék in gameObjects)
            {
                GameObject.Destroy(fluikék);
            }
        }

        //Move Ábel by touching the screen
        /*public void TouchMove()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                float middle = Screen.width / 2;

                if (touch.position.x < middle && touch.phase == TouchPhase.Began)
                {
                    Debug.Log("Left touch");
                    MoveLeft();
                }

                else if (touch.position.x > middle && touch.phase == TouchPhase.Began)
                {
                    Debug.Log("Right touch");
                    MoveRight();
                }
                else
                {
                    SetVelocityZero();
                }
            }
        }*/

        //Move based functions

        public void MoveLeft()
        {
            rb2D.velocity = new Vector2(maxSpeed * -1, 0);
            transform.localScale = new Vector3(-7, 8, 1);
        }

        public void MoveRight()
        {
            transform.localScale = new Vector3(7, 8, 1);
            rb2D.velocity = new Vector2(maxSpeed, 0);
            
        }

        public void SetVelocityZero()
        {
            rb2D.velocity = Vector2.zero;
        }
    }
}
