﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReviveCounter : MonoBehaviour {
    public static ReviveCounter instance;
    
    public int ReviveCount;
    Text text;

    // Use this for initialization
    void Start()
    {
        instance = this;
        text = GetComponent<Text>();

        ReviveCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = ReviveCount.ToString();
    }
}
