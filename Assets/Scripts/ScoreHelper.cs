﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreHelper : MonoBehaviour {
    public static ScoreHelper instance;

    //scroing system

    public int score;
    Text text;
   
    // Use this for initialization
	void Start ()
    {
        instance = this;
        text = GetComponent<Text>();

        score = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        text.text =  score.ToString();
	}
    
    public void Addpoints ()
    {
        score += 1;

        if (score <= 30 && score % 5 == 0)
        {
            FluiSpawner.instance.SpawnFluiKek();
        }
        else if(score > 30 && score % 10 == 0)
        {
            FluiSpawner.instance.SpawnFluiKek();
        }
    }

    public void Reset()
    {
       score = 0;
    }

    
}