﻿using UnityEngine;
using System.Collections;

public class KnifeHitAudio : MonoBehaviour {

    private AudioManager audioManager;

    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("AudioManager not found");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //knife hit
        if (other.gameObject.tag == "Player")
        {
            audioManager.PlaySound("cough");
        }

    }
}
