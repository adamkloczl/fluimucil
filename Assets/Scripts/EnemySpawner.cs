﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    public GameObject EnemyGO; //prefab

    float maxSpawnRateInSeconds = 2f;

	// Use this for initialization
	void Start ()
    {
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);
	}

    //function to spawn enemy
    void SpawnEnemy()
    {
        //this is the bottom-left point of the screen
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, -1));

        //this is the topright point of the screen
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 2));

        //instantiate an enemy
        GameObject anEnemy_1 = (GameObject)Instantiate(EnemyGO);
        anEnemy_1.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_2 = (GameObject)Instantiate(EnemyGO);
        anEnemy_2.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_3 = (GameObject)Instantiate(EnemyGO);
        anEnemy_3.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_4 = (GameObject)Instantiate(EnemyGO);
        anEnemy_4.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_5 = (GameObject)Instantiate(EnemyGO);
        anEnemy_5.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_6 = (GameObject)Instantiate(EnemyGO);
        anEnemy_6.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_7 = (GameObject)Instantiate(EnemyGO);
        anEnemy_7.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_8 = (GameObject)Instantiate(EnemyGO);
        anEnemy_8.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_9 = (GameObject)Instantiate(EnemyGO);
        anEnemy_9.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
        GameObject anEnemy_10 = (GameObject)Instantiate(EnemyGO);
        anEnemy_10.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        //schedule when to spawn next enemy
        ScheduleNextEnemySpawn();
    }

    void ScheduleNextEnemySpawn()
    {
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);
    }
}
